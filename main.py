from telegram import Bot
from telegram.ext import Updater

from db.db_config import engine, Base
from dispatcher.dispatcher import Dispatcher
from timer.timed_jobs import JobTimer

BALE_TOKEN = '121105964:9ea39d03cc1bb467e1504871f95d9396b1efee60'
BALE_BASE_URL = 'https://tapi.bale.ai/bot'
BALE_BASE_FILE_URL = 'https://tapi.bale.ai/file/bot'

if __name__ == '__main__':
    Base.metadata.create_all(engine)
    bot = Bot(token=BALE_TOKEN,
              base_url=BALE_BASE_URL,
              base_file_url=BALE_BASE_FILE_URL)
    updater = Updater(bot=bot)
    dispatcher = Dispatcher(updater=updater)
    job_timer = JobTimer(updater=updater)
    updater.start_webhook(clean=False,
                          listen='0.0.0.0',
                          port=8443,
                          url_path='',
                          # key='/home/mahdi/private.key',
                          # cert='/home/mahdi/cert.pem',
                          webhook_url='https://ee38f010.ngrok.io')
    updater.idle()

    # updater.start_polling()
