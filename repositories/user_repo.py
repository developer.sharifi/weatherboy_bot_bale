from typing import Optional, List

from sqlalchemy.orm import exc

from db.db_config import session
from db.db_models import DBUser
from domain_models import User


class UserRepo:
    def __init__(self):
        self._session = session

    def _db_model_to_entity(self, db_model: DBUser) -> Optional[User]:
        return User(id=db_model.id,
                    chat_id=db_model.chat_id,
                    latitude=db_model.latitude,
                    longitude=db_model.longitude,
                    timezone=db_model.timezone)

    def submit_user(self,
                    chat_id: int,
                    latitude: float,
                    longitude: float,
                    timezone: str) -> Optional[User]:
        db_user = self._get_dbuser_by_chat_id(chat_id=chat_id)
        if db_user:
            db_user.latitude = latitude
            db_user.longitude = longitude
            db_user.timezone = timezone
        else:
            db_user = DBUser(chat_id=chat_id,
                             latitude=latitude,
                             longitude=longitude,
                             timezone=timezone)
        self._session.add(db_user)
        self._session.commit()
        return self._db_model_to_entity(db_user)

    def _get_dbuser_by_chat_id(self, chat_id: int) -> Optional[DBUser]:
        try:
            db_user = self._session.query(DBUser).filter_by(chat_id=chat_id).one()
        except exc.NoResultFound:
            db_user = None

        return db_user

    def get_all_users(self) -> List[User]:
        db_users = self._session.query(DBUser).all()
        domain_users = []
        for a_db_user in db_users:
            domain_users.append(self._db_model_to_entity(a_db_user))
        return domain_users
