import requests


class GeoLocationRepo:
    def __init__(self):
        self._api_key = '0a09a7595fcc4b03a447df94f197eda3'

    def get_timezone_from_coordinates(self, lat: float, long:float):
        url = 'https://api.ipgeolocation.io/timezone?apiKey={api_key}&lat={lat}&long={long}'.format(
            api_key=self._api_key,
            lat=lat,
            long=long
        )
        response = requests.get(url=url)
        json = response.json()
        timezone = json.get('timezone', None)
        return timezone

if __name__=='__main__':
    geo_location_repo = GeoLocationRepo()
    geo_location_repo.get_timezone_from_coordinates(lat=1.0, long=1.0)