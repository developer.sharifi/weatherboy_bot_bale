import pyowm

from domain_models import Weather, Wind


class WeatherRepo:
    def __init__(self):
        appid = '45e5b26d6517f3871d852930b8478e4a'
        self._owm = pyowm.OWM(API_key=appid)

    def _convert_api_weather_to_domain_weather(self, city, weather):
        dtime = weather.get_reference_time(timeformat='date')
        status = weather.get_detailed_status()
        temperature = weather.get_temperature(unit='celsius').get('temp')
        wind = weather.get_wind()  # meter per second
        humidity = weather.get_humidity()
        domain_weather_model = Weather(dtime=dtime,
                                       city=city,
                                       status=status,
                                       temperature=temperature,
                                       wind=Wind(speed=wind.get('speed'), degree=wind.get('deg')),
                                       humidity=humidity)
        return domain_weather_model

    def get_current_weather_by_city(self, city: str) -> Weather:
        try:
            observation = self._owm.weather_at_place(name=city)
        except pyowm.exceptions.api_response_error.NotFoundError:
            return None
        location = observation.get_location()
        city = location.get_name()
        weather = observation.get_weather()
        domain_weather_model = self._convert_api_weather_to_domain_weather(city, weather)
        return domain_weather_model

    def get_five_day_forecast_by_city(self, city: str) -> [Weather]:
        try:
            forecast = self._owm.three_hours_forecast(name=city)
        except pyowm.exceptions.api_response_error.NotFoundError:
            return None
        forecast = forecast.get_forecast()
        city = forecast.get_location().get_name()
        domain__weather_models = []
        for weather in forecast.get_weathers()[::8]:
            domain_weather_model = self._convert_api_weather_to_domain_weather(city, weather)
            domain__weather_models.append(domain_weather_model)

        return domain__weather_models

    def get_five_day_forecast_by_lat_long(self, latitude: float, longitude: float) -> [Weather]:
        try:
            forecast = self._owm.three_hours_forecast_at_coords(lat=latitude, lon=longitude)
        except pyowm.exceptions.api_response_error.NotFoundError:
            return None
        forecast = forecast.get_forecast()
        city = forecast.get_location().get_name()
        domain__weather_models = []
        for weather in forecast.get_weathers()[::8]:
            domain_weather_model = self._convert_api_weather_to_domain_weather(city, weather)
            domain__weather_models.append(domain_weather_model)

        return domain__weather_models

# if __name__ == '__main__':
#     weather_repo = WeatherRepo()
#     weather_repo.get_five_day_forecast_by_lat_long(latitude=51.51, longitude=-0.13)
#     print(1)
