import os


class Config:
    db_user = os.getenv('POSTGRES_USER', "weatherboy")
    db_password = os.getenv('POSTGRES_PASSWORD', "123")
    db_host = os.getenv('POSTGRES_HOST', "localhost")
    db_name = os.getenv('POSTGRES_DB', "weatherboy_db")
    db_port = os.getenv('POSTGRES_PORT', "5432")
    database_url = "postgresql://{}:{}@{}:{}/{}".format(db_user, db_password, db_host, db_port, db_name)


from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(Config.database_url)
Session = sessionmaker(bind=engine)
Base = declarative_base()
session = Session()
